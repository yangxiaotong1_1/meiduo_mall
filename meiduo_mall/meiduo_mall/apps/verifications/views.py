import random

from django.http import HttpResponse
from django.shortcuts import render

from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from meiduo_mall.libs.captcha.captcha import captcha
from django_redis import get_redis_connection
import logging

from meiduo_mall.utils.yuntongxun.sms import CCP
from verifications.serializers import ImageCodeCheckSerializer
from . import constants
from celery_tasks.sms.tasks import send_sms_code


# Create your views here.
logger = logging.getLogger('django')


class ImageCodeView(APIView):
    """图片验证码"""
    """
        这里为什么可以继承APIView。因为这个视图逻辑不需要检验参数，参数由url路由中的正则就可以校验，所以压根不用考虑数据校验，不需要查询数据库，所以不需要做序列化操作。在DRF提供的类视图中只有APIView可以不用序列化器，其他的类视图都需要设置序列化器
    """
    def get(self, request, image_code_id):

        # 生成验证码图片
        text, image = captcha.generate_captcha()
        print("图片验证码为：%s" % text)

        # 保存真实值
        redis_conn = get_redis_connection('verify_codes')  # get_redis_connection获取redis链接对象的方法，参数为获取的是那个redis连接配置项
        redis_conn.setex("img_%s" % image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES, text)  # image_code_id指redis自己生成的用来保存生成的图片验证码的id；constants常量；IMAGE_CODE_REDIS_EXPIRES设置的过期时间，text图片内容

        # 返回图片给前端
        """
            为什么还能返回HttpResponse，不是应该返回Response对象么?
            因为Response是间接继承HttpResponse的，而视图本身就应该返回的是HttpResponse。在DRF中可以返回Response，主要是因为Response继承了HttpResponse
        
        """
        return HttpResponse(image, content_type='image/jpg')


# url('^sms_codes/(?P<mobile>1[3-9]\d{9})/$', views.SMSCodeView.as_view()),
class SMSCodeView(GenericAPIView):
    """
    短信验证码
    传入参数： mobile, image_code_id, text
    """
    serializer_class = ImageCodeCheckSerializer  # 声明所使用的序列化器

    def get(self, request, mobile):
        # 校验参数，由序列化器完成
        serializer = self.get_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        # 生成短信验证码
        sms_code = '%06d' % random.randint(0, 999999)
        print("短信验证码为：%s" % sms_code)

        # 保存短信验证码，发送记录
        redis_conn = get_redis_connection('verify_codes')  # 链接redis保存记录的数据库

        # redis_conn.setex("sms_%s" % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        # redis_conn.setex("send_flag_%s" % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)

        # redis管道
        pl = redis_conn.pipeline()
        pl.setex("sms_%s" % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        pl.setex("send_flag_%s" % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)

        # 让管道通知redis执行命令
        pl.execute()

        # # 发送短信
        # try:
        #     ccp = CCP()
        #     expires = constants.SMS_CODE_REDIS_EXPIRES // 60
        #     result = ccp.send_template_sms(mobile, [sms_code, expires], constants.SMS_CODE_TEMP_ID)
        #
        # except Exception as e:
        #     logger.error("发送验证码短信【异常】[ mobile: %s, message: %s]" % (mobile, e))
        #     return Response({'message': 'failed'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # else:
        #     if result == 0:
        #         logger.info("发送短信验证码短信[正常][mobile: %s]" % mobile)
        #         return Response({'message': 'OK'})
        #     else:
        #         logger.warning("发送短信验证码短信[失败][mobile: %s]" % mobile)
        #         return Response({'message': 'failed'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # 使用celery发送短信验证码
        expires = constants.SMS_CODE_REDIS_EXPIRES//60
        send_sms_code.delay(mobile, sms_code, expires, constants.SMS_CODE_TEMP_ID)

        return Response({'message': 'OK'})



