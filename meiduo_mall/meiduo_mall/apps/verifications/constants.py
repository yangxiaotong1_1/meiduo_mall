
# 图片验证码redis有效期，单位秒
IMAGE_CODE_REDIS_EXPIRES = 300

# 短信验证码有效期
SMS_CODE_REDIS_EXPIRES = 5*60

# 短信发送时间间隔
SEND_SMS_CODE_INTERVAL = 60

# 短信模板ID
SMS_CODE_TEMP_ID = 1
