from django.contrib import admin

# Register your models here.
from . import models


class SKUAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        """

        :param request: 保存的是当前视图的请求对象是谁
        :param obj: 修改后的对象
        :param form: 修改后返回的表单
        :param change: 当新建一个对象时change=False， 当修改一个对象时change=True
        :return:
        """
        obj.save()  # 做保存动作

        # 触发异步任务
        from celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html(obj.id)

    def delete_model(self, request, obj):
        obj.delete()


class SKUSpecificationAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        obj.save()
        from celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(obj.sku.id)

    def delete_model(self, request, obj):
        sku_id = obj.sku.id
        obj.delete()
        from celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(sku_id)


class SKUImageAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        # obj -> 就是 SKUImage对象

        # 生成静态页面
        obj.save()
        from celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(obj.sku.id)  # 调用异步任务

        # 设置SKU默认图片
        sku = obj.sku
        if not sku.default_image_url:
            # 拿到完整的路径信息：  http://image.meiduo.site:8888/groupxxxxxx
            sku.default_image_url = obj.image.url
            sku.save()

    def delete_model(self, request, obj):
        sku_id = obj.sku.id
        obj.delete()
        from celery_tasks.html.tasks import generate_static_sku_detail_html
        generate_static_sku_detail_html.delay(sku_id)


admin.site.register(models.GoodsCategory)
admin.site.register(models.GoodsChannel)
admin.site.register(models.Goods)
admin.site.register(models.Brand)
admin.site.register(models.GoodsSpecification)
admin.site.register(models.SpecificationOption)
admin.site.register(models.SKU, SKUAdmin)
admin.site.register(models.SKUSpecification, SKUSpecificationAdmin)
admin.site.register(models.SKUImage, SKUImageAdmin)
