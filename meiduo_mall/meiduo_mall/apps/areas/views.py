from django.shortcuts import render

# Create your views here.


# GET /areas/
# class AreasView(ListModelMixin, GenericAPIView):
#
#     def list(self):
#
#
#     def get(self):
#
#     #　查询Area数据（复数）
#
#     # 序列化返回
#         pass


# #以上可以简化为下面的方法
# class AreasView(ListAPIView):
#
#     pass
#
#
# # GET  /areas/<pk>
# class SubAreasView(RetrieveModelMixin, GenericAPIView):
#
#     def retrieve(self):
#
#
#     def get(self):
#         retrieve()
#
#
#     # 查询单一的数据对象
#
#     #　系列化返回
#
#
# class SubAreasView(RetrieveAPIView):
#     pass
#
# #以上可以简化为下面的方法
# class AreasViewSet(ListModelMixin,RetrieveModekMixin,  GenericViewSet):
#
#     pass

#以上可以简化为下面的方法
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework_extensions.cache.mixins import CacheResponseMixin

from areas import serializers
from areas.models import Area


class AreasViewSet(CacheResponseMixin, ReadOnlyModelViewSet):
    # queryset = Area.objects.all()
    # queryset = Area.objects.fiter(parent=None)
    # serializer_class =

    pagination_class = None   # 关闭分页处理

    def get_queryset(self):
        if self.action == 'list':
            return Area.objects.filter(parent=None)
        else:
            return Area.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.AreaSerializer
        else:
            return serializers.SubAreaSerializer

# /areas/  {'get': 'list'}  只返回顶级数据  parent=None
# /areas/<pk>  {'get': 'retrieve'}


