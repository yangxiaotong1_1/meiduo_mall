from rest_framework import serializers

from areas.models import Area


class AreaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Area
        fields = ('id', 'name')


# bj_area = Area.objects.get(id=1101000)
# bj_area.id
# bj_area.name
# bj_area.subs = [.....]


class SubAreaSerializer(serializers.ModelSerializer):
    subs = AreaSerializer(many=True, read_only=True)

    class Meta:
        model = Area
        fields = ('id', 'name', 'subs')

        # {
        #     "id": "110100",
        #     "name": "北京市",
        #     "subs": [
        #         {
        #             "id": "110101",
        #             "name": "东城区"
        #         },
        #         {
        #             "id": "110102",
        #             "name": "西城区"
        #         }
        #     ]
        # }


















