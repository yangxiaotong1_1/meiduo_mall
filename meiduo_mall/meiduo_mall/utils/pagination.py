from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 2  # 默认每页数量
    page_size_query_param = 'page_size'  # 前端访问指明哪个参数代表每页数量
    max_page_size = 20  # 限制最大每页数量
